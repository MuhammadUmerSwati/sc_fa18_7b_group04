
public class Accountwithunlimitedcredit extends BankAccount {
	public Accountwithunlimitedcredit(String title,String account,double ba) {
		super(title,account,ba);
	}
	public void withdraw(int withdraw)
	{
		if (withdraw < getBalance() && withdraw%500==0 )
		{
			setBalance(getBalance()-withdraw);
			System.out.println("Withdraw Successfully!");
		}
		else
		{
			System.out.println("Not enough balance or Wrong input!");
		}
	}
}
