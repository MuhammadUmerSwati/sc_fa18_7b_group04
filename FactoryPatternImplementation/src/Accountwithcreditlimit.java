
public class Accountwithcreditlimit extends BankAccount {
	public Accountwithcreditlimit(String title,String account,double ba) {
		super(title,account,ba);
	}
	
		private int creditLimit;
		
		public Accountwithcreditlimit()
		{
			creditLimit = 40000;
		}
		
		public Accountwithcreditlimit(int creditLimit)
		{
			this.creditLimit = creditLimit;
		}
		
		
		public void setCreditLimit(int creditLimit)
		{
			this.creditLimit = creditLimit;
		}
		
		
		public int getCreditLimit()
		{
			return creditLimit;
		}
		
		// withdraw money
		public void withdraw(int withdraw)
		{
			if (withdraw < getBalance() && withdraw%500==0 && withdraw <  20000 && withdraw<creditLimit)
			{
				withdraw -= creditLimit;
				setBalance(getBalance()-withdraw);
				System.out.println("Withdraw Successfully!");
			}else
			{
				System.out.println("Not enough balance or Wrong input!");
			}
		}
	
}
