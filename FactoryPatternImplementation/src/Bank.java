
public class Bank {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		BankAccountFactory bankaccount = new BankAccountFactory();
		AccountType simplewithdrawal = bankaccount.getBankAccount("simplewithdrawal","Muhammad Umer","MU01",50000);
		AccountType creditlimit = bankaccount.getBankAccount("creditlimit","Faizan Saeed","FS02",50000);
		AccountType unlimitedcredit = bankaccount.getBankAccount("unlimitedcredit","Zohaib Arshad","ZA03",50000);
		System.out.println("Test of Account With Simple WithDrawal");
		System.out.println("Current Balance:"+simplewithdrawal.getBalance());
		System.out.println("After WithDrawal Of 10000 Rupees!");
		simplewithdrawal.withdraw(10000);
		System.out.println("Current Balance:"+simplewithdrawal.getBalance());
		
		
	}

}
