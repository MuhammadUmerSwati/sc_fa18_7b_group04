
public class BankAccountFactory {
public AccountType getBankAccount(String at,String title,String acc,double balacne) {
	if(at == null) {
		return null;
	}
	if(at.equalsIgnoreCase("simplewithdrawal")) {
		return new Accountwithsimplewithdrawal(title,acc,balacne);
	}
	if(at.equalsIgnoreCase("creditlimit")) {
		return new Accountwithcreditlimit(title,acc,balacne);
	}
	if(at.equalsIgnoreCase("unlimitedcredit")) {
		return new Accountwithunlimitedcredit(title,acc,balacne);
	}
	return null;
}
}
