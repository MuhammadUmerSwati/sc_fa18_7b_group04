public class BankAccount implements AccountType {
	private String title;
	private String accNumber;
	protected double balance;
	
	public BankAccount() {
		
	}
	
	public BankAccount(String title, String accNumber, double balance)
	{
		this.title = title;
		this.accNumber = accNumber;
		this.balance = balance;
	}
	
	// setter for balance
	public void setBalance(double balance)
	{
		this.balance = balance;
	}
	
	// getter for balance
	public double getBalance()
	{
		return this.balance;
	}
	
	public void withdraw(int withdraw)
	{
		
	}
	
		
}